// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../ThreadMiddleGameModeBase.h"
#include "SimpleCubeActor.generated.h"

UCLASS(BlueprintType)
class THREADMIDDLE_API ASimpleCubeActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASimpleCubeActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	float StartedTime = 0.f;

	void Init(FNPCInfoStruct InitInfoNPConSpawn);

	UFUNCTION(BlueprintNativeEvent)
		void InitBP(FNPCInfoStruct InfoNPC);

	
	FThreadSafeCounter AgeCoub = FThreadSafeCounter(0);



	UFUNCTION(BlueprintCallable)
	int32 UpdateAgeCoub(int32 &CurrentTime);


		
};
