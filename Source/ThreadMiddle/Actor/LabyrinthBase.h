// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LabyrinthBase.generated.h"

DECLARE_DELEGATE(FTaskDelegate_CreateWals);

class ALabyrinthSection;

UCLASS()
class THREADMIDDLE_API ALabyrinthBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALabyrinthBase();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		int32 SizeX = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		int32 SizeY = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		bool bUseSleep = false;
	UPROPERTY(EditDefaultsOnly, Category = "Setting")
		TSubclassOf<ALabyrinthSection> CreatedSections = nullptr;

	FTaskDelegate_CreateWals OnTaskDelegate_CreateWals;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	TMap<FVector, bool> MapLabyrinth;
	TMap<FVector, bool> MapWay;
	TArray<ALabyrinthSection*> PoolSection;
	FCriticalSection MapLabyrinthMutex;
	FCriticalSection MapWaysMutex;
public:	
	bool SectionLoad(FVector VectorToCheck);
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void CreateLabyrinth(bool bReconsntruck);

	UFUNCTION()
	void CreateWals();

};
