// Fill out your copyright notice in the Description page of Project Settings.


#include "../Actor/LabyrinthBase.h"
#include "../Actor/LabyrinthSection.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
class FTask_CreateMapSection
{
	ALabyrinthBase* OwningActor;
	bool bUseSleep = false;
	FCriticalSection* MutexSection;
	TMap<FVector, bool>* MapLabyrinth;
	int32 SizeX;
	int32 SizeY;
	int32 CurrentIndexX;
	int32 CurrentIndexY;
	

public:
	FTask_CreateMapSection(ALabyrinthBase* InOwningActor,bool InUseSleep,FCriticalSection* InMutexSection,
	TMap<FVector, bool>* InMapLabyrinth,
	int32 InSizeX,
	int32 InSizeY,
	int32 InCurrentIndexX,
	int32 InCurrentIndexY) // CAUTION!: Must not use references in the constructor args; use pointers instead if you need by reference
		: OwningActor(InOwningActor),bUseSleep(InUseSleep),MutexSection(InMutexSection), MapLabyrinth(InMapLabyrinth), SizeX(InSizeX), SizeY(InSizeY), CurrentIndexX(InCurrentIndexX), CurrentIndexY(InCurrentIndexY)
	{
		//UE_LOG(LogTemp, Error, TEXT("FTask_Counter_Track::FTask_FinishWork()"))// Usually the constructor doesn't do anything except save the arguments for use in DoWork or GetDesiredThread.
	}
	~FTask_CreateMapSection()
	{
		//UE_LOG(LogTemp, Error, TEXT("FTask_Counter_Track::~FTask_FinishWork()"))// you will be destroyed immediately after you execute. Might as well do cleanup in DoWork, but you could also use a destructor.
	}
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FTask_CreateMapSection, STATGROUP_TaskGraphTasks);
	}

	static ENamedThreads::Type GetDesiredThread()
	{
		FAutoConsoleTaskPriority TaslPriority(TEXT("TaskGraph.TaskPriority.LoadFileToString"), TEXT("Task and thread priority for file loading"), ENamedThreads::BackgroundThreadPriority, ENamedThreads::NormalTaskPriority, ENamedThreads::BackgroundThreadPriority);
		return TaslPriority.Get();
	}

	static ESubsequentsMode::Type GetSubsequentsMode() { return ESubsequentsMode::TrackSubsequents; }
	void Work()
	{
		FVector Location = FVector(CurrentIndexX * 100.f, CurrentIndexY * 100.f, 0.f);
		bool IsLoad = false;
		{
			FScopeLock ScopeLock(MutexSection);
			IsLoad = OwningActor->SectionLoad(Location);
		}
		if (!IsLoad) // ���� �� ������
		{
			int8 Direction = UKismetMathLibrary::RandomIntegerInRange(0, 3);
			switch (Direction)
			{
			case 0://Left
				CurrentIndexY = CurrentIndexY - 1;
				if (CurrentIndexY > 0)
				{
					Location = FVector(CurrentIndexX * 100.f, (CurrentIndexY) * 100.f, 0.f);
					FVector Up = FVector((CurrentIndexX + 1) * 100.f, (CurrentIndexY) * 100.f, 0.f);
					FVector Down = FVector((CurrentIndexX - 1) * 100.f, (CurrentIndexY) * 100.f, 0.f);

					FScopeLock ScopeLock(MutexSection);
					MapLabyrinth->Add(Up, true);
					MapLabyrinth->Add(Down, true);
				}
				else
				{
					CurrentIndexY = CurrentIndexY + 2;
					Location = FVector(CurrentIndexX * 100.f, CurrentIndexY * 100.f, 0.f);
					FVector Up = FVector((CurrentIndexX + 1) * 100.f, CurrentIndexY * 100.f, 0.f);
					FVector Down = FVector((CurrentIndexX - 1) * 100.f, CurrentIndexY * 100.f, 0.f);

					FScopeLock ScopeLock(MutexSection);
					MapLabyrinth->Add(Up, true);
					MapLabyrinth->Add(Down, true);
				}
				break;
			case 1://Right
				CurrentIndexY = CurrentIndexY + 1;
				if (CurrentIndexY < SizeY - 2)
				{
					Location = FVector(CurrentIndexX * 100.f, (CurrentIndexY) * 100.f, 0.f);
					FVector Up = FVector((CurrentIndexX +1) * 100.f, (CurrentIndexY) * 100.f, 0.f);
					FVector Down = FVector((CurrentIndexX-1) * 100.f, (CurrentIndexY) * 100.f, 0.f);

					FScopeLock ScopeLock(MutexSection);
					MapLabyrinth->Add(Up, true);
					MapLabyrinth->Add(Down, true);
				}	
				else
				{
					CurrentIndexY = CurrentIndexY - 2;
					Location = FVector(CurrentIndexX * 100.f, CurrentIndexY * 100.f, 0.f);
					FVector Up = FVector((CurrentIndexX + 1) * 100.f, CurrentIndexY * 100.f, 0.f);
					FVector Down = FVector((CurrentIndexX - 1) * 100.f, CurrentIndexY * 100.f, 0.f);

					FScopeLock ScopeLock(MutexSection);
					MapLabyrinth->Add(Up, true);
					MapLabyrinth->Add(Down, true);
				}
				break;
			case 2://Up
				CurrentIndexX = CurrentIndexX + 1;
				if (CurrentIndexX < SizeX - 2)
				{
					Location = FVector((CurrentIndexX) * 100.f, (CurrentIndexY) * 100.f, 0.f);

					FVector Right = FVector((CurrentIndexX) * 100.f, (CurrentIndexY+1) * 100.f, 0.f);
					FVector Left = FVector((CurrentIndexX) * 100.f, (CurrentIndexY-1) * 100.f, 0.f);

					FScopeLock ScopeLock(MutexSection);
					MapLabyrinth->Add(Left, true);
					MapLabyrinth->Add(Right, true);
				}
				else
				{
					CurrentIndexX = CurrentIndexX - 2;
					Location = FVector((CurrentIndexX - 1) * 100.f, (CurrentIndexY) * 100.f, 0.f);

					FVector Right = FVector(CurrentIndexX * 100.f, (CurrentIndexY + 1) * 100.f, 0.f);
					FVector Left = FVector(CurrentIndexX * 100.f, (CurrentIndexY - 1) * 100.f, 0.f);

					FScopeLock ScopeLock(MutexSection);
					MapLabyrinth->Add(Left, true);
					MapLabyrinth->Add(Right, true);
				}
					
				break;
			case 3://Down
				CurrentIndexX = CurrentIndexX - 1;
				if (CurrentIndexX - 1 > 0)
				{
					Location = FVector((CurrentIndexX) * 100.f, (CurrentIndexY) * 100.f, 0.f);
					FVector Right = FVector((CurrentIndexX) * 100.f, (CurrentIndexY + 1) * 100.f, 0.f);
					FVector Left = FVector((CurrentIndexX) * 100.f, (CurrentIndexY - 1) * 100.f, 0.f);

					FScopeLock ScopeLock(MutexSection);
					MapLabyrinth->Add(Left, true);
					MapLabyrinth->Add(Right, true);
				}
				else
				{
					CurrentIndexX = CurrentIndexX + 2;
					Location = FVector((CurrentIndexX) * 100.f, (CurrentIndexY) * 100.f, 0.f);

					FVector Right = FVector((CurrentIndexX) * 100.f, (CurrentIndexY + 1) * 100.f, 0.f);
					FVector Left = FVector((CurrentIndexX) * 100.f, (CurrentIndexY - 1) * 100.f, 0.f);

					FScopeLock ScopeLock(MutexSection);
					MapLabyrinth->Add(Left, true);
					MapLabyrinth->Add(Right, true);
				}
					
				break;
			default:
				break;
			}
			
	
					//Work();

		}
		else //���� ������
		{

		}
	}
	void DoTask(ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent)
	{
		if (bUseSleep)
		{
			FPlatformProcess::Sleep(0.5);
		}
		// WALS CREATOR START
		if (CurrentIndexY == 0 || CurrentIndexY == SizeY-1)
		{
			FScopeLock ScopedLock(MutexSection);
			FVector Location = FVector(CurrentIndexX * 100.f, CurrentIndexY * 100.f, 0.f);
			MapLabyrinth->Add(Location, true);
		}
		else if (CurrentIndexX == 0 && CurrentIndexY > 0 && CurrentIndexY < SizeY-1 || CurrentIndexX == SizeX-1 && CurrentIndexY > 0 && CurrentIndexY < SizeY-1)
		{
			FScopeLock ScopedLock(MutexSection);
			FVector Location = FVector(CurrentIndexX * 100.f, CurrentIndexY * 100.f, 0.f);
			MapLabyrinth->Add(Location, true);
		} //WALS CREATOR END
		else //CREATOR LABYRINTH SECTION
		{
			int32 SizeWorkingAreaX = SizeX - 2;
			int32 SizeWorkingAreaY = SizeY - 2;
		
			Work();
			if ((CurrentIndexX == 1 && CurrentIndexY == 1) || (CurrentIndexX == SizeWorkingAreaX && CurrentIndexY == SizeWorkingAreaY))
			{
				FScopeLock ScopeLock(MutexSection);
				FVector Location = FVector(CurrentIndexX * 100.f, CurrentIndexY * 100.f, 0.f);
				MapLabyrinth->Add(Location, false);

			}
			
		}
	}
};



class FTask_WaysCheck
{
	ALabyrinthBase* OwningActor;
	FCriticalSection* MutexSection;
	TMap<FVector, bool>* MapLabyrinth;
	int32 SizeX;
	int32 SizeY;
	FCriticalSection* MapWaysMutex;
	TMap<FVector, bool>* MapWay;

	int32 IndexX = 1;
	int32 IndexY = 1;
	int32 IndexStep = 0;

	float CoefSize = 0;
public:
	FTask_WaysCheck(ALabyrinthBase* InOwningActor,
	FCriticalSection* InMutexSection,
	TMap<FVector, bool>* InMapLabyrinth,
		int32 InSizeX,
		int32 InSizeY, FCriticalSection* InMapWaysMutex,
	TMap<FVector, bool>* InMapWay) : OwningActor(InOwningActor), MutexSection(InMutexSection), MapLabyrinth(InMapLabyrinth), SizeX(InSizeX), SizeY(InSizeY), MapWaysMutex( InMapWaysMutex), MapWay(InMapWay) // CAUTION!: Must not use references in the constructor args; use pointers instead if you need by reference
	{
		//UE_LOG(LogTemp, Error, TEXT("FTask_Counter_Track::FTask_FinishWork()"))// Usually the constructor doesn't do anything except save the arguments for use in DoWork or GetDesiredThread.
	}
	~FTask_WaysCheck()
	{
		//UE_LOG(LogTemp, Error, TEXT("FTask_Counter_Track::~FTask_FinishWork()"))// you will be destroyed immediately after you execute. Might as well do cleanup in DoWork, but you could also use a destructor.
	}
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FTask_WaysCheck, STATGROUP_TaskGraphTasks);
	}

	static ENamedThreads::Type GetDesiredThread()
	{
		return ENamedThreads::AnyThread;
	}

	static ESubsequentsMode::Type GetSubsequentsMode() { return ESubsequentsMode::TrackSubsequents; }

	void CheckStep()
	{
		int32 Steps = 0;
		int32 Direction = 0;
		int32 LocalDirectionX = 0;
		int32 LocalDirectionY = 0;
		switch (IndexStep)
		{
		case 0:
			Steps = 2* CoefSize + CoefSize - 1;
			Direction = 1;
			break;
		case 1:
			Steps = 2 * CoefSize + CoefSize - 1;
			Direction = 2;
			break;
		case 2:
			Steps = 2 * CoefSize + CoefSize - 1;
			Direction = 3;
			break;
		case 3:
			Steps = 2 * CoefSize;
			Direction = 2;
			break;
		case 4:
			Steps = 4 * CoefSize + CoefSize - 1;
			Direction = 1;
			break;
		case 5:
			Steps = 4 * CoefSize + CoefSize - 1;
			Direction = 0;
			break;
		case 6:
			Steps = 2 * CoefSize ;
			Direction = 1;
			break;
		case 7:
			Steps = 6 * CoefSize + CoefSize - 1;
			Direction = 2;
			break;
		case 8:
			Steps = 6 * CoefSize + CoefSize - 1;
			Direction = 3;
			break;
		case 9:
			Steps = 2 * CoefSize ;
			Direction = 2;
			break;
		case 10:
			Steps = 1 * CoefSize + CoefSize - 1;
			Direction = 1;
			break;
		case 11:
			Steps = 2 * CoefSize;
			Direction = 2;
			break;
		case 12:
			Steps = 2 * CoefSize;
			Direction = 1;
			break;
		case 13:
			Steps = 1 * CoefSize + CoefSize - 1;
			Direction = 0;
			break;
		case 14:
			Steps = 2 * CoefSize;
			Direction = 1;
			break;
		case 15:
			Steps = 1 * CoefSize + CoefSize - 1;
			Direction = 2;
			break;
		case 16:
			Steps = 2 * CoefSize;
			Direction = 1;
			break;
		case 17:
			Steps = 3* CoefSize + CoefSize - 1;
			Direction = 0;
			break;
		case 18:
			Steps = 1 * CoefSize;
			Direction = 1;
			break;
		case 19:
			Steps = 7 * CoefSize;
			Direction = 0;
			break;
		case 20:
			Steps = 2 * CoefSize;
			Direction = 1;
			break;
		case 21:
			Steps = 10 * CoefSize + CoefSize - 1;
			Direction = 2;
			break;
		default:
			break;
		}

		switch (Direction)
			{
			case 0://Left
				LocalDirectionY = -1;
				break;
			case 1://Up
				LocalDirectionX = +1;
				break;
			case 2://Right
				LocalDirectionY = +1;
				break;
			case 3://Down
				LocalDirectionX = -1;
				break;
			default:
				break;
			}

		for (int i = 0; i < Steps; i++)
		{
			IndexX = IndexX + LocalDirectionX;
			IndexY = IndexY + LocalDirectionY;
			if(IndexStep==1)
			UE_LOG(LogTemp, Error, TEXT("Ways Check Start, %i, %i"), IndexX, IndexY);
			FVector Location = FVector(IndexX * 100, IndexY * 100, 0);


			FScopeLock ScopeLock(MutexSection);
			if (OwningActor->SectionLoad(Location))
			{
				MapLabyrinth->Add(Location, false);
			}

		}
		if (IndexStep +1< 22)
		{
			IndexStep++;
			CheckStep();
		}
	}
	void DoTask(ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent)
	{
		CoefSize = (SizeX-2) / 11;
		UE_LOG(LogTemp, Error, TEXT("Ways Check Start, %i"), CoefSize);
		CheckStep();
		UE_LOG(LogTemp, Error, TEXT("Ways Complete"));

	}
};

class FTask_CreateSectionComplete
{
	FTaskDelegate_CreateWals TaskDelegate_CreateWals;
public:
	FTask_CreateSectionComplete(FTaskDelegate_CreateWals InTaskDelegate_CreateWals) :TaskDelegate_CreateWals(InTaskDelegate_CreateWals) // CAUTION!: Must not use references in the constructor args; use pointers instead if you need by reference
	{
		//UE_LOG(LogTemp, Error, TEXT("FTask_Counter_Track::FTask_FinishWork()"))// Usually the constructor doesn't do anything except save the arguments for use in DoWork or GetDesiredThread.
	}
	~FTask_CreateSectionComplete()
	{
		//UE_LOG(LogTemp, Error, TEXT("FTask_Counter_Track::~FTask_FinishWork()"))// you will be destroyed immediately after you execute. Might as well do cleanup in DoWork, but you could also use a destructor.
	}
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FTask_CreateSectionComplete, STATGROUP_TaskGraphTasks);
	}

	static ENamedThreads::Type GetDesiredThread()
	{
		return ENamedThreads::GameThread;
	}

	static ESubsequentsMode::Type GetSubsequentsMode() { return ESubsequentsMode::TrackSubsequents; }

	void DoTask(ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent)
	{
		check(IsInGameThread())
		{
			if (TaskDelegate_CreateWals.IsBound())
			{
				TaskDelegate_CreateWals.Execute();
			}
		}
		UE_LOG(LogTemp, Error, TEXT("Build Complete"));
	}

};

// Sets default values
ALabyrinthBase::ALabyrinthBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ALabyrinthBase::BeginPlay()
{
	SizeX = SizeX * 11+2;
	SizeY = SizeY * 11+2;
	Super::BeginPlay();

	int32 SpawnCountSectionToPool = SizeX * SizeY * 0.65f;
	int32 i = 0;
	while (i < SpawnCountSectionToPool)
	{
		FActorSpawnParameters SpawnParam;
		SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		ALabyrinthSection* SectionToPool = GetWorld()->SpawnActor<ALabyrinthSection>(CreatedSections, FVector(0), FRotator(0), SpawnParam);
		if (SectionToPool)
		{
			PoolSection.Add(SectionToPool);
			SectionToPool->SetActorHiddenInGame(true);
			SectionToPool->SetActorEnableCollision(false);
		}
		i++;
	}
		
	
}

bool ALabyrinthBase::SectionLoad(FVector VectorToCheck)
{
	if (MapLabyrinth.Find(VectorToCheck))
		return *MapLabyrinth.Find(VectorToCheck) || MapLabyrinth.Contains(VectorToCheck);
	else
		return false;
}

// Called every frame
void ALabyrinthBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALabyrinthBase::CreateLabyrinth(bool bReconsntruck)
{
	if (!bReconsntruck)
	{
		FGraphEventArray prerequilsites;
		for (int32 i = 0; i < SizeX; i++)
		{
			for (int32 j = 0; j < SizeY; j++)
			{
				FGraphEventRef CounterTask = TGraphTask<FTask_CreateMapSection>::CreateTask(nullptr, ENamedThreads::AnyThread).ConstructAndDispatchWhenReady(this, bUseSleep, &MapLabyrinthMutex, &MapLabyrinth, SizeX, SizeY, i, j);
				prerequilsites.Add(CounterTask);
			}
		}
		
			FGraphEventRef WayTask = TGraphTask<FTask_WaysCheck>::CreateTask(nullptr, ENamedThreads::AnyThread).ConstructAndDispatchWhenReady(this, &MapLabyrinthMutex, &MapLabyrinth, SizeX, SizeY, &MapWaysMutex, &MapWay);
			prerequilsites.Add(WayTask);
	
			
		
		//TGraphTask<FTask_WaysCheck>::CreateTask(&prerequilsites, ENamedThreads::GameThread).ConstructAndDispatchWhenReady();
		OnTaskDelegate_CreateWals.BindUFunction(this, "CreateWals");
		TGraphTask<FTask_CreateSectionComplete>::CreateTask(&prerequilsites, ENamedThreads::GameThread).ConstructAndDispatchWhenReady(OnTaskDelegate_CreateWals);
	}
	else
	{
		MapLabyrinth.Empty();
		TArray<AActor*> AllSection;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), CreatedSections, AllSection);
		for (auto i : AllSection)
		{
			PoolSection.Add(Cast<ALabyrinthSection>(i));
			i->SetActorLocation(FVector(0));
			i->SetActorHiddenInGame(true);
			i->SetActorEnableCollision(false);
		}
			

		FGraphEventArray prerequilsites;
		for (int32 i = 0; i < SizeX; i++)
		{
			for (int32 j = 0; j <SizeY ; j++)
			{
				FGraphEventRef CounterTask = TGraphTask<FTask_CreateMapSection>::CreateTask(nullptr, ENamedThreads::AnyThread).ConstructAndDispatchWhenReady(this,bUseSleep, &MapLabyrinthMutex, &MapLabyrinth, SizeX, SizeY, i, j);
				prerequilsites.Add(CounterTask);
			}
		}
		FGraphEventRef WayTask = TGraphTask<FTask_WaysCheck>::CreateTask(nullptr, ENamedThreads::AnyThread).ConstructAndDispatchWhenReady(this, &MapLabyrinthMutex, &MapLabyrinth, SizeX, SizeY, &MapWaysMutex, &MapWay);
		prerequilsites.Add(WayTask);
		OnTaskDelegate_CreateWals.BindUFunction(this, "CreateWals");
		TGraphTask<FTask_CreateSectionComplete>::CreateTask(&prerequilsites, ENamedThreads::GameThread).ConstructAndDispatchWhenReady(OnTaskDelegate_CreateWals);
	}
		
	
}

void ALabyrinthBase::CreateWals()
{
	TArray<FVector> Keys;
	MapLabyrinth.GetKeys(Keys);

	for (auto Key : Keys)
	{
		bool IsSpawn = *MapLabyrinth.Find(Key);
		if (IsSpawn)
		{
			if (PoolSection.Num() > 0)
			{
				PoolSection[0]->SetActorLocationAndRotation(Key, GetActorRotation());
				PoolSection[0]->SetActorHiddenInGame(false);
				PoolSection[0]->SetActorEnableCollision(true);
				PoolSection.RemoveAt(0);
			}
			else
			{
				FActorSpawnParameters SpawnParam;
				SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				ALabyrinthSection* Section = GetWorld()->SpawnActor<ALabyrinthSection>(CreatedSections, Key, GetActorRotation(), SpawnParam);
			}
		}
	}
	
}

