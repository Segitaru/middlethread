// Fill out your copyright notice in the Description page of Project Settings.


#include "../Actor/LabyrinthSection.h"

// Sets default values
ALabyrinthSection::ALabyrinthSection()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StaticMeshSection = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Section Labyrinth"));
	SetRootComponent(StaticMeshSection);
}

// Called when the game starts or when spawned
void ALabyrinthSection::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALabyrinthSection::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

