// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LabyrinthSection.generated.h"

UCLASS()
class THREADMIDDLE_API ALabyrinthSection : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ALabyrinthSection();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Component")
		UStaticMeshComponent* StaticMeshSection;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
