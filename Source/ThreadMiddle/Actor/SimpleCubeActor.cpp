// Fill out your copyright notice in the Description page of Project Settings.



#include "SimpleCubeActor.h"

// Sets default values
ASimpleCubeActor::ASimpleCubeActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASimpleCubeActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASimpleCubeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASimpleCubeActor::Init(FNPCInfoStruct InitInfoNPConSpawn)
{
	StartedTime = InitInfoNPConSpawn.StartedTime;
	InitBP(InitInfoNPConSpawn);
	
}

void ASimpleCubeActor::InitBP_Implementation(FNPCInfoStruct InfoNPC)
{
}


int32 ASimpleCubeActor::UpdateAgeCoub(int32& CurrentTime)
{
	return AgeCoub.GetValue();
}






