// Copyright Epic Games, Inc. All Rights Reserved.


#include "ThreadMiddleGameModeBase.h"

#include "SynPrim/SimpleAtomic_Runnable.h"
#include "SynPrim/SimpleCounter_Runnable.h"

#include "SynPrim/SimpleMutex_Runnable.h"
#include "SynPrim/SimpleCollector_Runnable.h"
#include "SynPrim/Custom/SimpleMutexAgeCount_Runnable.h"


#include "Actor/SimpleCubeActor.h"

#include "Kismet/GameplayStatics.h"

#include "MessageEndpointBuilder.h"

void AThreadMiddleGameModeBase::BusMessageHandler_NameGenerator(const FBusStructMessage_NameGenerator& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context)
{

	EventMessage_NameGenerator(Message.bIsSecond, Message.TextName);
}

void AThreadMiddleGameModeBase::BusMessageHandler_NPCInfo(const FNPCInfoStruct& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context)
{
	EventMessage_NPCInfo(Message);
}

void AThreadMiddleGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	/* Show Thread ID, Name and Priority*/
	if (bShowThreadNameAndPriority)
	{
		uint32 ThreadID = FPlatformTLS::GetCurrentThreadId();


		UE_LOG(LogTemp, Warning, TEXT("AThreadMiddleGameModeBase::Tick ID - %d, Name Thread - %s "), ThreadID, *FThreadManager::GetThreadName(ThreadID));

		
		FThreadManager::Get().ForEachThread([&](uint32 ThreadID, FRunnableThread* Runnable)
			{
				FString PriorityThread = "none";
				switch (Runnable->GetThreadPriority())
				{
				case TPri_Normal:
					PriorityThread = "TPri_Normal";
					break;
				case TPri_AboveNormal:
					PriorityThread = "TPri_AboveNormal";
					break;
				case TPri_BelowNormal:
					PriorityThread = "TPri_BelowNormal";
					break;
				case TPri_Highest:
					PriorityThread = "TPri_Highest";
					break;
				case TPri_Lowest:
					PriorityThread = "TPri_Lowest";
					break;
				case TPri_SlightlyBelowNormal:
					PriorityThread = "TPri_SlightlyBelowNormal";
					break;
				case TPri_TimeCritical:
					PriorityThread = "TPri_TimeCritical";
					break;
				case TPri_Num:
					PriorityThread = "TPri_Num";
					break;
				default:
					break;
				}

				UE_LOG(LogTemp, Warning, TEXT("AThreadMiddleGameModeBase::Tick ID - %d, Name Thread - %s, Priority - %s "), ThreadID, *FThreadManager::GetThreadName(ThreadID), *PriorityThread);
			});

	}


}

void AThreadMiddleGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	RecieveEndpoint_NameGenerator = FMessageEndpoint::Builder("Reciever_AThreadMiddleGameModeBase").Handling<FBusStructMessage_NameGenerator>(this, &AThreadMiddleGameModeBase::BusMessageHandler_NameGenerator);
	if (RecieveEndpoint_NameGenerator.IsValid())
		RecieveEndpoint_NameGenerator->Subscribe<FBusStructMessage_NameGenerator>();

	RecieveEndpoint_NPCInfo = FMessageEndpoint::Builder("RecieverCollector_AThreadMiddleGameModeBase").Handling<FNPCInfoStruct>(this, &AThreadMiddleGameModeBase::BusMessageHandler_NPCInfo);
	if (RecieveEndpoint_NPCInfo.IsValid())
		RecieveEndpoint_NPCInfo->Subscribe<FNPCInfoStruct>();
}

void AThreadMiddleGameModeBase::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	StopSimpleCounter();
	StopSimpleMutexThread();
	if (RecieveEndpoint_NameGenerator.IsValid())
	{
		RecieveEndpoint_NameGenerator.Reset();
	}
	if (RecieveEndpoint_NPCInfo.IsValid())
	{
		RecieveEndpoint_NPCInfo.Reset();
	}
}

void AThreadMiddleGameModeBase::CreateSimpleAtomicThread()
{
	for (size_t i = 0; i < ThreadToCreate; i++)
	{
		bool bIsSeparateLogicLocal = false;
			if (i % 2)
				bIsSeparateLogicLocal = true;
			FColor Color;

			class FSimpleAtomic_Runnable* RunnableClass_SimpleAtomic = new FSimpleAtomic_Runnable(Color, this, ItterationCount, bIsSeparateLogicLocal, bUseSimpleAtomic);
			CurrentRunnableThread_SimpleAtomic.Add(FRunnableThread::Create(RunnableClass_SimpleAtomic, TEXT("Simple Atomic"), 0, EThreadPriority::TPri_Lowest));

	}
	
}
void AThreadMiddleGameModeBase::GetCounters_SimpleAtomic(int32& Atomic1, int32& Atomic2, int32& NonAtomic1, int32& NonAtomic2)
{
	if (bUseSimpleAtomic)
	{
		NonAtomic1 = FPlatformAtomics::AtomicRead(&NonAtomicCounter_1);
		NonAtomic2 = FPlatformAtomics::AtomicRead(&NonAtomicCounter_2);
		if (AtomicCounter_1.is_lock_free())
		{
			Atomic1 = AtomicCounter_1.load();
		}
		if (AtomicCounter_2.is_lock_free())
		{
			Atomic2 = AtomicCounter_2.load();
		}
	}
	else
	{
		NonAtomic1 = NonAtomicCounter_1;
		NonAtomic2 = NonAtomicCounter_2;
	}
	

}
void AThreadMiddleGameModeBase::ResetCounter()
{
	AtomicCounter_1 = 0;
	AtomicCounter_2 = 0;
	NonAtomicCounter_1 = 0;
	NonAtomicCounter_2 = 0;
}

void AThreadMiddleGameModeBase::StopSimpleCounter()
{
	if (CurrentRunnableThread_SimpleCounter)
	{
		if (CurrentSimpleCounter_Runnable)
		{
			CurrentRunnableThread_SimpleCounter->Suspend(false); //Cancel pause, if thread in this state

			if (SimpleCounterEvent)
			{
				SimpleCounterEvent->Trigger();
				FPlatformProcess::ReturnSynchEventToPool(SimpleCounterEvent);//recomed by Epic Games
				SimpleCounterEvent = nullptr;
			}

			if (SimpleCounterScopedEvent_Ref)
			{
				SimpleCounterScopedEvent_Ref->Trigger();
				SimpleCounterScopedEvent_Ref = nullptr;
			}

			CurrentSimpleCounter_Runnable->bIsStopThread = true;
			CurrentSimpleCounter_Runnable->bIsStopThreadSafe.AtomicSet(true);

			
			
		}
		CurrentRunnableThread_SimpleCounter->WaitForCompletion();
		CurrentRunnableThread_SimpleCounter = nullptr;
		CurrentSimpleCounter_Runnable = nullptr;
	}
}

void AThreadMiddleGameModeBase::KillSimpleCounter(bool bIsShouldToWait)
{
	if (CurrentRunnableThread_SimpleCounter)
	{
		CurrentRunnableThread_SimpleCounter->Suspend(false);
		CurrentRunnableThread_SimpleCounter->Kill(bIsShouldToWait);
		CurrentRunnableThread_SimpleCounter = nullptr;
		CurrentSimpleCounter_Runnable = nullptr;
	}
}

void AThreadMiddleGameModeBase::CreateSimpleCounterThread()
{
	if (bIsUseEvent)
	{
		SimpleCounterEvent = FPlatformProcess::GetSynchEventFromPool();
	}

	if (!CurrentRunnableThread_SimpleCounter)
	{
		if (!CurrentSimpleCounter_Runnable)
		{
			CurrentSimpleCounter_Runnable = new FSimpleCounter_Runnable(SimpleCounterColor, this, bUseSafeVariable);
		}
		CurrentRunnableThread_SimpleCounter = FRunnableThread::Create(CurrentSimpleCounter_Runnable, TEXT("Simple Counter"), 0, EThreadPriority::TPri_Lowest);
	}
}

bool AThreadMiddleGameModeBase::SwitchRunStateSimpleCounterThread(bool bIsPause)
{
	if (CurrentRunnableThread_SimpleCounter)
	{
		CurrentRunnableThread_SimpleCounter->Suspend(bIsPause);
	}
	return !bIsPause;
}

int64 AThreadMiddleGameModeBase::GetCounters_SimpleCounter()
{
	int64 result = 0;
	if (CurrentSimpleCounter_Runnable)
	{
		if (CurrentSimpleCounter_Runnable->bIsUseSaveVariable)
		{
			result = CurrentSimpleCounter_Runnable->SafeCounter.GetValue();
		}
		else
		{
			result = CurrentSimpleCounter_Runnable->Counter;
		}
	}
		
	return result;
}

void AThreadMiddleGameModeBase::TriggerSimpleCounterThreadWithEvent()
{
	if (SimpleCounterEvent)
	{
		SimpleCounterEvent->Trigger();
		//SimpleCounterEvent = nullptr; //better use nullptr after use, if have a lot thread with guard waiting this event
	}
}

void AThreadMiddleGameModeBase::TriggerSimpleCounterThreadWithScopedEvent()
{
	if (SimpleCounterScopedEvent_Ref)
	{
		SimpleCounterScopedEvent_Ref->Trigger();
		SimpleCounterScopedEvent_Ref = nullptr; //ALL TIME
	}
}

void AThreadMiddleGameModeBase::CreateSimpleMutexThread()
{
	for (int32 i = 0; i < NumberThread_SimpleMutex; i++)
	{
		bool bDevideLogic = false;
		if (i % 2)
			bDevideLogic = true;

		class FSimpleMutex_Runnable* RunnableMutex = new FSimpleMutex_Runnable(SimpleMutexColor, this, bDevideLogic);
		CurrentArrayRunningGameModeThread_SimpleMutex.Add(FRunnableThread::Create(RunnableMutex, TEXT("Simple Mutex"), 0, EThreadPriority::TPri_Lowest));

	}
}

void AThreadMiddleGameModeBase::CreateSimpleCollectorThread()
{
	class FSimpleCollector_Runnable* RunnableCollector = new FSimpleCollector_Runnable(SimpleMutexColor, this);
	CurrentArrayRunningGameModeThread_SimpleMutex.Add(FRunnableThread::Create(RunnableCollector, TEXT("Simple Mutex"), 0, EThreadPriority::TPri_Lowest));
}

void AThreadMiddleGameModeBase::StopSimpleMutexThread()
{
	if (CurrentArrayRunningGameModeThread_SimpleMutex.Num() > 0)
	{
		for (auto RunnableThread : CurrentArrayRunningGameModeThread_SimpleMutex)
		{
			if (RunnableThread)
			{
				RunnableThread->Kill(true);
			}
		}
		CurrentArrayRunningGameModeThread_SimpleMutex.Empty();
	}
	if (CurrentRunningGameModeThread_SimpleMutex)
	{
		CurrentRunningGameModeThread_SimpleMutex->Kill(true);
		CurrentRunningGameModeThread_SimpleMutex = nullptr;
	}
		
}

TArray<FString> AThreadMiddleGameModeBase::GetSecondsName()
{
	TArray<FString> result;
	FString LocalSecondName = "none";
	while (SecondName.Dequeue(LocalSecondName))
	{
		result.Add(LocalSecondName);
	}
	CurrentSecondName.Append(result);
	return result;
}
TArray<FString> AThreadMiddleGameModeBase::GetFirstNames()
{
	{
		FScopeLock LockScope(&FirstName_Mutex);
		return FirstName;
	}
	
}

TArray<FNPCInfoStruct> AThreadMiddleGameModeBase::GetNPCInfo()
{
	TArray<FNPCInfoStruct> result;
	return result;
}

float AThreadMiddleGameModeBase::GetCurrentTime()
{
	return UGameplayStatics::GetRealTimeSeconds(GetWorld());
}

void AThreadMiddleGameModeBase::EventMessage_NameGenerator(bool bIsSecond, FString StringData)
{
	OnUpdateByNameGeneratorThreads.Broadcast(bIsSecond, StringData);
}

void AThreadMiddleGameModeBase::EventMessage_NPCInfo(FNPCInfoStruct NPCInfoData)
{
	OnUpdateByThreadNPC.Broadcast(NPCInfoData);

	UWorld* MyWorld = GetWorld();
	if (MyWorld && CubeClass)
	{
		FVector SpawnLoc = FVector(1200.f, 100.f * CountCube + CountCube *30.f, 500.f);
		FRotator SpawnRot;
		ASimpleCubeActor* Cube;
			Cube = Cast<ASimpleCubeActor>(MyWorld->SpawnActor(CubeClass, &SpawnLoc, &SpawnRot, FActorSpawnParameters()));
		if (Cube)
		{
			Cube->Init(NPCInfoData);
			CountCube++;

			//Age Controll for Cube
			FSimpleMutexAgeCount_Runnable* CurrentSimpleAgeCount_Runnable = new FSimpleMutexAgeCount_Runnable(SimpleCounterColor, this, Cube);
			CurrentArrayRunningGameModeThread_SimpleMutex.Add(FRunnableThread::Create(CurrentSimpleAgeCount_Runnable, TEXT("Simple Age Controll"), 0, EThreadPriority::TPri_Lowest));
		}
			
	}
}

void AThreadMiddleGameModeBase::StartParallel_1()
{
	FCriticalSection ParallelMutex; 
	ParallelFor(10,[&](int32 index)
		{
			FPlatformProcess::Sleep(0.01f);
			int32 count = 0;
			for (int i = 0; i < 50; i++)
			{
				count++;
			}
			ParallelMutex.Lock();
			CountParallel_1 += count;
			ParallelMutex.Unlock();
		}
	, EParallelForFlags::None);
}

void AThreadMiddleGameModeBase::StartParallel_2()
{
	const auto Function = [&](int32 index)
	{
		FPlatformProcess::Sleep(0.2f);
		int32 count = 0;
		for (int i = 0; i < 50; i++)
		{
			count++;
		}

		CountParallel_2 += count;
	};

	ParallelForTemplate(10, Function, EParallelForFlags::BackgroundPriority);
}

void AThreadMiddleGameModeBase::StartParallel_3()
{
	ParallelForWithPreWork(10, [&](int32 index)
		{
			for (int i = 0; i < 50; i++)
			{
				CountParallel_3++;
				UE_LOG(LogTemp, Error, TEXT("Start ParallelForWithPreWork"));
			}

		}, []()
		{
			UE_LOG(LogTemp, Error, TEXT("Help Work - ParallelForWithPreWork")); 
			FPlatformProcess::Sleep(5.f);
			UE_LOG(LogTemp, Error, TEXT("Help Work End - ParallelForWithPreWork"));
		}, EParallelForFlags::BackgroundPriority);
	
		
}
