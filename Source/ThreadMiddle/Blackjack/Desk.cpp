// Fill out your copyright notice in the Description page of Project Settings.


#include "../Blackjack/Desk.h"
#include "../Blackjack/Card.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "../Blackjack/Framework/BJGameModeBase.h"
#include "../Blackjack/Framework/BJGameStateBase.h"
// Sets default values
ADesk::ADesk()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
//	AvaibleRankCard = { ERank::Ace, ERank::Two, ERank::Three, ERank::Fourth, ERank::Five, ERank::Six, ERank::Seven, ERank::Eight, ERank::Nine, ERank::Ten, ERank::Jack, ERank::Lady, ERank::King };
//	AvaibleSuitCard = { ESuit::Clubs, ESuit::Diamonds,ESuit::Hearts, ESuit::Spades };
}

// Called when the game starts or when spawned
void ADesk::BeginPlay()
{
	Super::BeginPlay();
	ABJGameModeBase* GameModeRef = Cast< ABJGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	if (GameModeRef)
	{
		GameModeRef->CurrentDesk = this;
	}

	CreateDeckCard();
}

// Called every frame
void ADesk::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADesk::CreateDeckCard()
{
	FVector ActorLocation = GetActorLocation();
	FRotator ActorRotation = GetActorRotation();
	for (auto LocalSuit : AvaibleSuitCard)
	{
		for (auto LocalRank : AvaibleRankCard)
		{
			if (DeckCard.Num() > 0)
			{
				ActorLocation = FVector(ActorLocation.X, ActorLocation.Y, ActorLocation.Z + 0.01 * DeckCard.Num());
			}
			FActorSpawnParameters SpawnParam;
			SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			ACard* NewCard = GetWorld()->SpawnActor<ACard>(CardClass, ActorLocation, ActorRotation, SpawnParam);
			if (NewCard)
			{
				NewCard->InitCard(LocalRank, LocalSuit);
				DeckCard.Add(NewCard);
			}
		}
	}

	ShuffleDeck();
}

ACard* ADesk::GetUpCardDeck()
{
	if (DeckCard.Num() > 0)
	{
		ACard* LocalCard = DeckCard[DeckCard.Num() - 1];
		DeckCard.RemoveAt(DeckCard.Num() - 1);
		return LocalCard;
	}
	else
	{
		return nullptr;
	}
	
}

void ADesk::ReturnCardToDeck()
{
	ABJGameModeBase* GameModeRef = Cast< ABJGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	if (GameModeRef)
	{
		ABJGameStateBase* GameState = GameModeRef->GetGameState();
		TArray<ACard*> PlayerHand = GameState->GetPlayerHand();
		FVector ActorLocation = GetActorLocation();
		for (ACard* Card : PlayerHand)
		{
			DeckCard.Add(Card);
			Card->SwapStart(FVector(ActorLocation.X, ActorLocation.Y, ActorLocation.Z + 0.01 * DeckCard.Num()));
			Card->SetActorRotation(GetActorRotation());
		}

		TArray<ACard*> DillerHand = GameState->GetDillerHand();
		
		for (ACard* Card : DillerHand)
		{
			DeckCard.Add(Card);
			Card->SwapStart(FVector(ActorLocation.X, ActorLocation.Y, ActorLocation.Z + 0.01 * DeckCard.Num()));
			Card->SetActorRotation(GetActorRotation());
		}
		GameState->ClearingHand();

		ShuffleDeck();
	}
}

void ADesk::ShuffleDeck()
{
	if (DeckCard.Num() < 52)
	{
		ReturnCardToDeck();
	}

	for (ACard* i : DeckCard)
	{
		int32 RandomIndexToSwap = UKismetMathLibrary::RandomIntegerInRange(0, DeckCard.Num()-1);
		ACard* SwappingCard = DeckCard[RandomIndexToSwap];
		if (SwappingCard)
		{
			if (i == SwappingCard)
			{
				if (DeckCard.IsValidIndex(RandomIndexToSwap + 1))
				{
					RandomIndexToSwap++;
				}
				else
				{
					RandomIndexToSwap--;
				}
			}
			//For visual effect Shuffle Deck
			FVector PossitionCurrentCard = i->GetActorLocation();
			FVector PossitionSwappingCard	= SwappingCard->GetActorLocation();
			i->SwapStart(PossitionSwappingCard);
			SwappingCard->SwapStart(PossitionCurrentCard);
			int32 index = 0;
			DeckCard.Find(i, index);
			DeckCard.Swap(index, RandomIndexToSwap);
		}
		
	}
	
}

