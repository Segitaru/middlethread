// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "BJGameModeBase.generated.h"


class ADesk;
class ABJGameStateBase;
UCLASS()
class THREADMIDDLE_API ABJGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	

protected:
	virtual void BeginPlay() override;
	bool bGameInProgress = false;
	ABJGameStateBase* GameStateRef;
public:
	ABJGameStateBase* GetGameState() { return GameStateRef; };
	
	UPROPERTY()
		ADesk* CurrentDesk;
	UFUNCTION(BlueprintCallable)
		void TakeCard();
	UFUNCTION(BlueprintCallable)
		void StopTake();

	UFUNCTION(BlueprintCallable)
		void StartGame();


	UFUNCTION()
		void GameEnd(EResultGame ResultGame, int32 PlayerPoints, int32 DillerPoints);
};
