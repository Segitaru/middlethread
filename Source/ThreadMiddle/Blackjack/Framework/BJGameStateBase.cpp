// Fill out your copyright notice in the Description page of Project Settings.


#include "BJGameStateBase.h"
#include "../Card.h"
#include "../Framework/BJGameModeBase.h"

void ABJGameStateBase::ClearingHand()
{
	PlayerHand = TArray<ACard*>();
	DillerHand = TArray<ACard*>();
}

void ABJGameStateBase::ComparePoint()
{
	int32 DillerPoints = CheckPointsInHand(DillerHand);
	int32 PlayerPoints = CheckPointsInHand(PlayerHand);
	EResultGame GameResult;
	if (PlayerPoints > DillerPoints)
		GameResult = EResultGame::Win;
	else if(PlayerPoints < DillerPoints)
		GameResult = EResultGame::Lose;
	else
		GameResult = EResultGame::Draw;

	OnGameEnd.Broadcast(GameResult, PlayerPoints, DillerPoints);
}

int32 ABJGameStateBase::CheckPointsInHand(TArray<ACard*> InHand)
{
	int32 CurrentPoint = 0;
	for (ACard* CurrentCard : InHand)
	{
		ERank RankCurrentCard = CurrentCard->GetCardRank();
		
		switch (RankCurrentCard)
		{
		case ERank::Two:
			CurrentPoint = CurrentPoint + 2;
			break;
		case ERank::Three:
			CurrentPoint = CurrentPoint + 3;
			break;
		case ERank::Fourth:
			CurrentPoint = CurrentPoint + 4;
			break;
		case ERank::Five:
			CurrentPoint = CurrentPoint + 5;
			break;
		case ERank::Six:
			CurrentPoint = CurrentPoint + 6;
			break;
		case ERank::Seven:
			CurrentPoint = CurrentPoint + 7;
			break;
		case ERank::Eight:
			CurrentPoint = CurrentPoint + 8;
			break;
		case ERank::Nine:
			CurrentPoint = CurrentPoint + 9;
			break;
		case ERank::Ten:
			CurrentPoint = CurrentPoint + 10;
			break;
		case ERank::Jack:
			CurrentPoint = CurrentPoint + 10;
			break;
		case ERank::Lady:
			CurrentPoint = CurrentPoint + 10;
			break;
		case ERank::King:
			CurrentPoint = CurrentPoint + 10;
			break;
		case ERank::Ace:
			if (CurrentPoint + 11 <= 21)
				CurrentPoint = CurrentPoint + 11;
			else
				CurrentPoint = CurrentPoint + 1;
			break;
		default:
			break;
		}
	}


	return CurrentPoint;
}

void ABJGameStateBase::AddCardToPlayerHand(ACard* NewCard)
{
	ACard* CurrentCard = NewCard;
	PlayerHand.Add(CurrentCard);
	int32 CurrentPoints = CheckPointsInHand(PlayerHand);
	OnPointsInHandChange.Broadcast(CurrentPoints);
	if (CurrentPoints == 21)
	{
		OnGameEnd.Broadcast(EResultGame::Win, CurrentPoints, 0);
		UE_LOG(LogTemp, Error, TEXT("WIN"));
	}
	else if (CurrentPoints < 21)
	{
		UE_LOG(LogTemp, Error, TEXT("Some, %i"), CurrentPoints);
	}
	else
	{
		OnGameEnd.Broadcast(EResultGame::Lose, CurrentPoints, 0);
		UE_LOG(LogTemp, Error, TEXT("Lose"));
	}

}

void ABJGameStateBase::AddCardToDillerHand(ACard* NewCard)
{
	DillerHand.Add(NewCard);
	int32 CurrentPoints = CheckPointsInHand(DillerHand);
	if (CurrentPoints == 21)
	{
		UE_LOG(LogTemp, Error, TEXT("Diller WIN"));
	}
	else if (CurrentPoints < 21)
	{
		UE_LOG(LogTemp, Error, TEXT("Diller Some, %i"), CurrentPoints);
	}
	else
	{
		int32 PlayerPoints = CheckPointsInHand(PlayerHand);
		OnGameEnd.Broadcast(EResultGame::Win, PlayerPoints, CurrentPoints);
		UE_LOG(LogTemp, Error, TEXT("Diller Lose"));
	}
}
