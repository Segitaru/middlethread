// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "BJGameStateBase.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnGameEnd, EResultGame, ResultGame, int32, PlayerPoints, int32, DillerPoints);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPointsInHandChange,int32, PlayerPoints);

class ACard;

UCLASS()
class THREADMIDDLE_API ABJGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
	
	TArray<ACard*> PlayerHand;
	TArray<ACard*> DillerHand;
public:
	TArray<ACard*> GetPlayerHand() { return PlayerHand; };
	TArray<ACard*> GetDillerHand() { return DillerHand; };
	UPROPERTY(BlueprintAssignable)
	FOnGameEnd OnGameEnd;
	UPROPERTY(BlueprintAssignable)
	FOnPointsInHandChange OnPointsInHandChange;
	void ClearingHand();

	void ComparePoint();

	int32 CheckPointsInHand(TArray<ACard*> InHand);

	void AddCardToPlayerHand(ACard* NewCard);
	void AddCardToDillerHand(ACard* NewCard);


};
