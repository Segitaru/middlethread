// Fill out your copyright notice in the Description page of Project Settings.


#include "BJGameModeBase.h"
#include "../Desk.h"
#include "../Card.h"
#include "Kismet/GameplayStatics.h"
#include "../Framework/BJGameStateBase.h"

void ABJGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	GameStateRef = Cast< ABJGameStateBase>(UGameplayStatics::GetGameState(GetWorld()));
	GameStateRef->OnGameEnd.AddDynamic(this, &ABJGameModeBase::GameEnd);
	
}

void ABJGameModeBase::TakeCard()
{
	if (bGameInProgress)
	{
		ACard* UpCard = CurrentDesk->GetUpCardDeck();
		GameStateRef->AddCardToPlayerHand(UpCard);
	}
	
}

void ABJGameModeBase::StopTake()
{
	while (bGameInProgress && GameStateRef->CheckPointsInHand(GameStateRef->GetDillerHand())<17)
	{
		ACard* UpCard = CurrentDesk->GetUpCardDeck();
		GameStateRef->AddCardToDillerHand(UpCard);
	}
	if (bGameInProgress)
	{
		GameStateRef->ComparePoint();
	}

}

void ABJGameModeBase::StartGame()
{
	if (CurrentDesk->CheckDeskCardFull())
	{
		ACard* UpCard = CurrentDesk->GetUpCardDeck();
		GameStateRef->AddCardToDillerHand(UpCard);
	}
	else
	{
		CurrentDesk->ReturnCardToDeck();

		ACard* UpCard = CurrentDesk->GetUpCardDeck();
		GameStateRef->AddCardToDillerHand(UpCard);
	}
	bGameInProgress = true;
}

void ABJGameModeBase::GameEnd(EResultGame EResultGame, int32 PlayerPoints, int32 DillerPoints)
{
	bGameInProgress = false;
}
