// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Blackjack/Data/FuncLibrary.h"
#include "Card.generated.h"


UCLASS(BlueprintType)
class THREADMIDDLE_API ACard : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACard();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	ERank CardRank;
	ESuit CardSuit;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void InitCard(ERank InCardRank, ESuit InCardSuit);
	void SwapStart(FVector InNewPossition);
	UFUNCTION(BlueprintNativeEvent)
		void SwapStartBP(FVector InNewPossition);
	ERank GetCardRank() { return CardRank; };
	ESuit GetCardSuit() { return CardSuit; };
};
