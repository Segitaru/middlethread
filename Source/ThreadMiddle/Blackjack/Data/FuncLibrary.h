// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FuncLibrary.generated.h"

UENUM(BlueprintType)
enum class ESuit : uint8
{
	Spades UMETA(DisplayName = "Spades"),
	Hearts UMETA(DisplayName = "Hearts"),
	Diamonds UMETA(DisplayName = "Diamonds"),
	Clubs UMETA(DisplayName = "Clubs")
};

UENUM(BlueprintType)
enum class ERank : uint8
{
	Two UMETA(DisplayName = "2"),
	Three UMETA(DisplayName = "3"),
	Fourth UMETA(DisplayName = "4"),
	Five UMETA(DisplayName = "5"),
	Six UMETA(DisplayName = "6"),
	Seven UMETA(DisplayName = "7"),
	Eight UMETA(DisplayName = "8"),
	Nine UMETA(DisplayName = "9"),
	Ten UMETA(DisplayName = "10"),
	Jack UMETA(DisplayName = "Jack"),
	Lady UMETA(DisplayName = "Lady"),
	King UMETA(DisplayName = "King"),
	Ace UMETA(DisplayName = "Ace")
};

UENUM(BlueprintType)
enum class EResultGame : uint8
{
	Win UMETA(DisplayName = "Win"),
	Lose UMETA(DisplayName = "Lose"),
	Draw UMETA(DisplayName = "Draw"),
	
};

UCLASS()
class THREADMIDDLE_API UFuncLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
