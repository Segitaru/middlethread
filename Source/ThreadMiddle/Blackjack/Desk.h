// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Blackjack/Data/FuncLibrary.h"
#include "Desk.generated.h"

class ACard;
UCLASS(BlueprintType)
class THREADMIDDLE_API ADesk : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADesk();


	UPROPERTY(EditDefaultsOnly, Category = "Core")
		TSubclassOf<ACard> CardClass;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	TArray<ACard*> DeckCard;
	TArray<ERank> AvaibleRankCard = {ERank::Ace, ERank::Two, ERank::Three, ERank::Fourth, ERank::Five, ERank::Six, ERank::Seven, ERank::Eight, ERank::Nine, ERank::Ten, ERank::Jack, ERank::Lady, ERank::King};
	TArray<ESuit> AvaibleSuitCard = {ESuit::Clubs, ESuit::Diamonds,ESuit::Hearts, ESuit::Spades };
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void CreateDeckCard();
	
	ACard* GetUpCardDeck();

	bool CheckDeskCardFull() { return DeckCard.Num() == 52; };
	void ReturnCardToDeck();

	void ShuffleDeck();
};
