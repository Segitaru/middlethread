// Fill out your copyright notice in the Description page of Project Settings.


#include "../Blackjack/Card.h"


// Sets default values
ACard::ACard()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void ACard::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACard::InitCard(ERank InCardRank, ESuit InCardSuit)
{
	CardRank = InCardRank;
	CardSuit = InCardSuit;
}

void ACard::SwapStart(FVector InNewPossition)
{
	SwapStartBP(InNewPossition);
}

void ACard::SwapStartBP_Implementation(FVector InNewPossition)
{

}

