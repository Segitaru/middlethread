
#include "../Custom/SimpleMutexAgeCount_Runnable.h"

#include "ThreadMiddle/Actor/SimpleCubeActor.h"
#include "MessageEndpoint.h"
#include "MessageEndpointBuilder.h"

FSimpleMutexAgeCount_Runnable::FSimpleMutexAgeCount_Runnable(FColor Color, AThreadMiddleGameModeBase* OwnerThread, ASimpleCubeActor* ActorToControllAge)
{
	ControllableActorByAge = ActorToControllAge;
	GameMode_Ref = OwnerThread;
}

FSimpleMutexAgeCount_Runnable::~FSimpleMutexAgeCount_Runnable()
{
	
}

bool FSimpleMutexAgeCount_Runnable::Init()
{
	
	return true;
}

uint32 FSimpleMutexAgeCount_Runnable::Run()
{
	while (!bIsStopControllAge)
	{
		FPlatformProcess::Sleep(1.f);
		if (ControllableActorByAge)
		{
			int32 Age = GameMode_Ref->GetCurrentTime() - ControllableActorByAge->StartedTime;
			ControllableActorByAge->AgeCoub.Set(Age);
			//ControllableActorByAge->UpdateAgeCoub(ControllableActorByAge->AgeCoub.GetValue());
		}
	
	}
	
	return uint32();
}

void FSimpleMutexAgeCount_Runnable::Stop()
{
	bIsStopControllAge = true;
}

void FSimpleMutexAgeCount_Runnable::Exit()
{
	ControllableActorByAge = nullptr;
	GameMode_Ref = nullptr;
	
}