#pragma once

#include "CoreMinimal.h"
#include "ThreadMiddle/ThreadMiddleGameModeBase.h"


class THREADMIDDLE_API FSimpleMutexAgeCount_Runnable : public FRunnable	
{
public:
	FSimpleMutexAgeCount_Runnable(FColor Color, AThreadMiddleGameModeBase* OwnerThread, ASimpleCubeActor* ActorToControllAge);
	virtual ~FSimpleMutexAgeCount_Runnable() override;

	virtual bool Init() override;
	virtual uint32 Run() override;
	virtual void Stop() override;
	virtual void Exit() override;

	AThreadMiddleGameModeBase* GameMode_Ref = nullptr;
	
	bool bIsCurrentSecondMetod = false;
	FThreadSafeBool bIsStopControllAge = FThreadSafeBool(false);
	ASimpleCubeActor* ControllableActorByAge;


};

