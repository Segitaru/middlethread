
#include "../SynPrim/SimpleCollector_Runnable.h"
#include "MessageEndpoint.h"
#include "MessageEndpointBuilder.h"

FSimpleCollector_Runnable::FSimpleCollector_Runnable(FColor Color, AThreadMiddleGameModeBase* OwnerThread)
{
	
	GameMode_Ref = OwnerThread;
	SenderEndPoint = FMessageEndpoint::Builder("Sender_FSimpleCollector_Runnable").Build();
}

FSimpleCollector_Runnable::~FSimpleCollector_Runnable()
{

}

bool FSimpleCollector_Runnable::Init()
{
	
	return true;
}

uint32 FSimpleCollector_Runnable::Run()
{
	int32 i = 0;
	while (!bIsStopNameCollector)
	{
		FPlatformProcess::Sleep(2.f);

		FNPCInfoStruct NewNPC;
		NewNPC.Id = i;
		i++;
		
		int32 SizeName = GameMode_Ref->FirstName.Num(); //not safe, but careful
		if (SizeName > 0)
		{
			int32 RandNameIndex = FMath::RandHelper(SizeName - 1);
			GameMode_Ref->FirstName_Mutex.Lock(); 
			NewNPC.Name = GameMode_Ref->FirstName[RandNameIndex];
			GameMode_Ref->FirstName.RemoveAt(RandNameIndex);
			GameMode_Ref->FirstName_Mutex.Unlock();
		}

		TArray<FString> AvaibleSecondNames = GameMode_Ref->GetSecondsName();
		int32 SizeSecondName = AvaibleSecondNames.Num();
		if (SizeSecondName > 0)
		{
			int32 RandNameIndex = FMath::RandHelper(SizeSecondName - 1);
			NewNPC.SecondName = AvaibleSecondNames[RandNameIndex];
			GameMode_Ref->CurrentSecondName.Remove(NewNPC.SecondName);
		}

		NewNPC.NewColor = FColor(FMath::RandHelper(255), FMath::RandHelper(255), FMath::RandHelper(255), FMath::RandHelper(255));
		NewNPC.StartedTime = GameMode_Ref->GetCurrentTime();

		{
			FScopeLock NPCLockedScope(&GameMode_Ref->NPCName_Mutex);
			GameMode_Ref->NPCInfo.Add(NewNPC);
		}

		if (SenderEndPoint.IsValid())
		{
			SenderEndPoint->Publish<FNPCInfoStruct>(new FNPCInfoStruct(NewNPC));
		}
	}
	return uint32();
}

void FSimpleCollector_Runnable::Stop()
{
	bIsStopNameCollector = true;
}

void FSimpleCollector_Runnable::Exit()
{
	if (SenderEndPoint.IsValid())
	{
		SenderEndPoint.Reset();
	}
	GameMode_Ref = nullptr;
}
