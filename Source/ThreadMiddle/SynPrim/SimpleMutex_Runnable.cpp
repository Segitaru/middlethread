
#include "../SynPrim/SimpleMutex_Runnable.h"
#include <random>
#include "MessageEndpoint.h"
#include "MessageEndpointBuilder.h"

FSimpleMutex_Runnable::FSimpleMutex_Runnable(FColor Color, AThreadMiddleGameModeBase* OwnerThread, bool bIsSecondMode)
{
	
	GameMode_Ref = OwnerThread;
	bIsCurrentSecondMetod = bIsSecondMode;
	SenderEndPoint = FMessageEndpoint::Builder("Sender_FSimpleMutex_Runnable").Build();
}

FSimpleMutex_Runnable::~FSimpleMutex_Runnable()
{
	
}

bool FSimpleMutex_Runnable::Init()
{
	
	return true;
}

uint32 FSimpleMutex_Runnable::Run()
{
	TArray<FString> VowelLetter{ "a","e","i","o","u", "y"};
	TArray<FString> ConsonantLetter{ "b","c","d","k","l", "m", "n", "g","k"};

	while (!bIsStopNameGenerator)
	{
		FString Result= "";
		bool bIsStartVowel = GetRandomBool();
		int8 RandomSizeName = 0;
		if (bIsCurrentSecondMetod)
		{
			RandomSizeName = GetRandomValue(5, 8);
		}
		else
		{
			RandomSizeName = GetRandomValue(3, 5);
		}

		for (int i=0; i < RandomSizeName; i++)
		{
			bool bFlipFlop = false;
				if (i % 2)
				{
					bFlipFlop = false;
				}
				if (bIsStartVowel)
				{
					if (bFlipFlop)
					{
						Result.Append(VowelLetter[GetRandomValue(0, VowelLetter.Num() - 1)]);
					}
					else
					{
						Result.Append(ConsonantLetter[GetRandomValue(0, VowelLetter.Num() - 1)]);
					}
				}
				else
				{
					if (bFlipFlop)
					{
						Result.Append(ConsonantLetter[GetRandomValue(0, VowelLetter.Num() - 1)]);
					}
					else
					{
						Result.Append(VowelLetter[GetRandomValue(0, VowelLetter.Num() - 1)]);
					}
				}
		}

		FPlatformProcess::Sleep(1.5f);
		if(bIsCurrentSecondMetod)
		{
			GameMode_Ref->SecondName.Enqueue(Result);
		}
		else
		{
			//Dont safe variable! For safe using mutex!
			GameMode_Ref->FirstName_Mutex.Lock();
			GameMode_Ref->FirstName.Add(Result);
			GameMode_Ref->FirstName_Mutex.Unlock();
		}
		if (SenderEndPoint.IsValid())
			SenderEndPoint->Publish<FBusStructMessage_NameGenerator>(new FBusStructMessage_NameGenerator(bIsCurrentSecondMetod, Result));
	}

	return uint32();
}

void FSimpleMutex_Runnable::Stop()
{
	bIsStopNameGenerator = true;
}

void FSimpleMutex_Runnable::Exit()
{
	if (SenderEndPoint.IsValid())
	{
		SenderEndPoint.Reset();
	}
	GameMode_Ref = nullptr;
	
}

int8 FSimpleMutex_Runnable::GetRandomValue(int8 min, int8 max)
{
	std::random_device rt;
	std::mt19937 gen(rt());
	std::uniform_int_distribution<> distribution(min,max);
	return distribution(gen);
}

bool FSimpleMutex_Runnable::GetRandomBool()
{
	std::random_device rt;
	std::mt19937 gen(rt());
	std::bernoulli_distribution d(0.5f);
	return d(gen);
}
