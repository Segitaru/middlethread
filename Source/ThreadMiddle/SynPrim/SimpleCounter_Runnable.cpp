#include "../SynPrim/SimpleCounter_Runnable.h"


FSimpleCounter_Runnable::FSimpleCounter_Runnable(FColor Color, AThreadMiddleGameModeBase* OwnerThread, bool VariableMode)
{
	GameMode_Ref = OwnerThread;
	bIsUseSaveVariable = VariableMode;
}

FSimpleCounter_Runnable::~FSimpleCounter_Runnable()
{
}

bool FSimpleCounter_Runnable::Init()
{
	return true;
}
//if run this class, he not can out from circle Run, for debug you can use #pragma optimize("", off) and #pragma optimize("", on), after function
uint32 FSimpleCounter_Runnable::Run()
{

	if (GameMode_Ref->bIsUseScopedEvent)
	{
		{
			FScopedEvent SimpleCounterScopedEvent;
			GameMode_Ref->SendRef_ScopedEvent(SimpleCounterScopedEvent);
		}
	}
	//FEvent
	if (GameMode_Ref->SimpleCounterEvent)
	{

		GameMode_Ref->SimpleCounterEvent->Wait(10000.f);
		if (GameMode_Ref->SimpleCounterEvent)
		{
			GameMode_Ref->SimpleCounterEvent->Wait(10000.f);
		}

	}

	if (bIsUseSaveVariable)
	{
		while (!bIsStopThreadSafe)
		{
			SafeCounter.Increment();
		}
	}
	else
	{
		while (!bIsStopThread)
		{
			Counter++;
		}
	}
	
	return uint32();
}

void FSimpleCounter_Runnable::Stop()
{
	bIsStopThread = true;
	bIsStopThreadSafe.AtomicSet(true);
	
}

void FSimpleCounter_Runnable::Exit()
{
	GameMode_Ref = nullptr;
}
