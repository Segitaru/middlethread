
#include "../SynPrim/SimpleAtomic_Runnable.h"


FSimpleAtomic_Runnable::FSimpleAtomic_Runnable(FColor Color, AThreadMiddleGameModeBase* OwnerThread, uint32 ItterationNum, bool bIsSeparateLogic, bool bIsUseAtomic)
{
	UE_LOG(LogTemp, Warning, TEXT("FSimpleAtomic_Runnable::FSimpleAtomic_Runnable()"));
	GameMode_Ref = OwnerThread;
	NumOfItteration = ItterationNum;
	bIsUseAtomicFlag = bIsUseAtomic;
	bSeparateLogic = bIsSeparateLogic;
}

FSimpleAtomic_Runnable::~FSimpleAtomic_Runnable()
{
	UE_LOG(LogTemp, Warning, TEXT("FSimpleAtomic_Runnable::~FSimpleAtomic_Runnable()"));
}

bool FSimpleAtomic_Runnable::Init()
{
	UE_LOG(LogTemp, Warning, TEXT("FSimpleAtomic_Runnable::Init()"));
	return true;
}

uint32 FSimpleAtomic_Runnable::Run()
{
	UE_LOG(LogTemp, Warning, TEXT("FSimpleAtomic_Runnable::Run()"));
	//FPlatformProcess::Sleep(1.f);
	for (uint32 i = 0; i < NumOfItteration; i++)
	{
		if (bSeparateLogic)
		{
			if (bIsUseAtomicFlag)
			{
				GameMode_Ref->AtomicCounter_1.fetch_add(1);
				FPlatformAtomics::InterlockedIncrement(&GameMode_Ref->NonAtomicCounter_1);
			}
			else
			{
				GameMode_Ref->NonAtomicCounter_1++;
			}
		}
		else
		{
			if (bIsUseAtomicFlag)
			{
				GameMode_Ref->AtomicCounter_2.fetch_add(1);
				FPlatformAtomics::InterlockedIncrement(&GameMode_Ref->NonAtomicCounter_2);
			}
			else
			{
				GameMode_Ref->NonAtomicCounter_2++;
			}
		}
		
		
	}
	return uint32();
}

void FSimpleAtomic_Runnable::Stop()
{
	UE_LOG(LogTemp, Warning, TEXT("FSimpleAtomic_Runnable::Stop()"));
}

void FSimpleAtomic_Runnable::Exit()
{
	GameMode_Ref = nullptr;
	UE_LOG(LogTemp, Warning, TEXT("FSimpleAtomic_Runnable::Exit()"));
}
