#pragma once

#include "CoreMinimal.h"
#include "ThreadMiddle/ThreadMiddleGameModeBase.h"


class THREADMIDDLE_API FSimpleMutex_Runnable : public FRunnable	
{
public:
	FSimpleMutex_Runnable(FColor Color, AThreadMiddleGameModeBase* OwnerThread, bool bIsSecondMode);
	virtual ~FSimpleMutex_Runnable() override;

	virtual bool Init() override;
	virtual uint32 Run() override;
	virtual void Stop() override;
	virtual void Exit() override;

	AThreadMiddleGameModeBase* GameMode_Ref = nullptr;
	
	bool bIsCurrentSecondMetod = false;
	FThreadSafeBool bIsStopNameGenerator = FThreadSafeBool(false);


	//TRUE RANDOM
	int8 GetRandomValue(int8 min, int8 max);
	bool GetRandomBool();

	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> SenderEndPoint;
};

