#pragma once

#include "CoreMinimal.h"
#include "ThreadMiddle/ThreadMiddleGameModeBase.h"


class THREADMIDDLE_API FSimpleCollector_Runnable : public FRunnable	
{
public:
	FSimpleCollector_Runnable(FColor Color, AThreadMiddleGameModeBase* OwnerThread);
	virtual ~FSimpleCollector_Runnable() override;

	virtual bool Init() override;
	virtual uint32 Run() override;
	virtual void Stop() override;
	virtual void Exit() override;


	
	AThreadMiddleGameModeBase* GameMode_Ref = nullptr;
	FThreadSafeBool bIsStopNameCollector = FThreadSafeBool(false);

	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> SenderEndPoint;
};

