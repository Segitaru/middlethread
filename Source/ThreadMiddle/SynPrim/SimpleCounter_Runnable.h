#pragma once

#include "CoreMinimal.h"
#include "ThreadMiddle/ThreadMiddleGameModeBase.h"


class THREADMIDDLE_API FSimpleCounter_Runnable : public FRunnable
{
public:
	FSimpleCounter_Runnable(FColor Color, AThreadMiddleGameModeBase* OwnerThread, bool VariableMode);
	virtual ~FSimpleCounter_Runnable() override;

	virtual bool Init() override;
	virtual uint32 Run() override;
	virtual void Stop() override;
	virtual void Exit() override;

	//Safe variable
	FThreadSafeBool bIsStopThreadSafe = FThreadSafeBool(false);
	FThreadSafeCounter SafeCounter = FThreadSafeCounter(0);
	//Not Save Variable!!!
	bool bIsStopThread = false;
	int64 Counter = 0;

	AThreadMiddleGameModeBase* GameMode_Ref = nullptr;
	bool bIsUseSaveVariable = true;
};

