#pragma once

#include "CoreMinimal.h"
#include "ThreadMiddle/ThreadMiddleGameModeBase.h"


class THREADMIDDLE_API FSimpleAtomic_Runnable : public FRunnable	
{
public:
	FSimpleAtomic_Runnable(FColor Color, AThreadMiddleGameModeBase* OwnerThread, uint32 ItterationNum, bool bIsSeparateLogic, bool bIsUseAtomic);
	virtual ~FSimpleAtomic_Runnable() override;

	virtual bool Init() override;
	virtual uint32 Run() override;
	virtual void Stop() override;
	virtual void Exit() override;


	uint32 NumOfItteration = 0;
	AThreadMiddleGameModeBase* GameMode_Ref = nullptr;
	bool bIsStopThread = false;
	bool bIsUseAtomicFlag = true;
	bool bSeparateLogic = false;
};

