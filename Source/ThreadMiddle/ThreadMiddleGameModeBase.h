// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "IMessageBus.h"
#include "MessageEndpoint.h"

#include "HAL/ThreadingBase.h"


#include "ThreadMiddleGameModeBase.generated.h"

/*Atomic struct can implement by simple variable, but on operation was complite 
all variable struct override*/
struct CustomAtomic
{
	int32 i;
	char a;
	bool bIsFlag;
};

USTRUCT(BlueprintType)
struct FNPCInfoStruct
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Id = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Name = "none";
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString SecondName = "none";
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FColor NewColor = FColor();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float StartedTime = 0.f;

};

USTRUCT()
struct FBusStructMessage_NameGenerator
{
	GENERATED_BODY()

		bool bIsSecond = false;

		FString TextName = "none";
		FBusStructMessage_NameGenerator(bool bInBoom = false, FString Text = "none") :bIsSecond(bInBoom), TextName(Text) {}

};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateByThreadNPC, FNPCInfoStruct, DataNPC);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateByNameGeneratorThreads, bool, bIsSecond, FString, StringData);

UCLASS(BlueprintType)
class THREADMIDDLE_API AThreadMiddleGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable)
		FOnUpdateByThreadNPC OnUpdateByThreadNPC;
	UPROPERTY(BlueprintAssignable)
		FOnUpdateByNameGeneratorThreads OnUpdateByNameGeneratorThreads;


	void BusMessageHandler_NameGenerator(const struct FBusStructMessage_NameGenerator& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);
	void BusMessageHandler_NPCInfo(const struct FNPCInfoStruct& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);

	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> RecieveEndpoint_NameGenerator;
	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> RecieveEndpoint_NPCInfo;

	virtual void Tick(float DeltaTime) override;
	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;

	// FLAG

	/* Show Thread ID, Name and Priority*/
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	bool bShowThreadNameAndPriority = false;


	// FLAG

	TArray<FRunnableThread*> CurrentRunnableThread_SimpleAtomic;
	// Simple Atomic Settings
	UPROPERTY(BlueprintReadWrite, Category = "Simple Atomic")
		int32 ItterationCount = 100000;
	UPROPERTY(BlueprintReadWrite, Category = "Simple Atomic")
		int32 ThreadToCreate = 12;
	UPROPERTY(BlueprintReadWrite, Category = "Simple Atomic")
		bool bUseSimpleAtomic = true;
	// Simple Atomic Settings

	//Simple Atomic Thread Controll

	/*This implementation simple multythread operation by Atomic class*/
	UFUNCTION(BlueprintCallable)
		void CreateSimpleAtomicThread();
	UFUNCTION(BlueprintPure)
		void GetCounters_SimpleAtomic(int32& Atomic1, int32& Atomic2, int32& NonAtomic1, int32& NonAtomic2);
	UFUNCTION(BlueprintCallable)
		void ResetCounter();
	//Simple Atomic Thread Controll

	//Simple Atomic Storage
	std::atomic_int16_t AtomicCounter_1 = 0;
	std::atomic_int16_t AtomicCounter_2 = 0;
	int16 NonAtomicCounter_1 = 0;
	int16 NonAtomicCounter_2 = 0;

	//Dont use, only rewiew
	std::atomic<CustomAtomic> CustomStruct_Atomic;
	//Simple Atomic Storage

	//Simple Counter Setting
	class FSimpleCounter_Runnable* CurrentSimpleCounter_Runnable = nullptr;
	FRunnableThread* CurrentRunnableThread_SimpleCounter = nullptr;
	UPROPERTY(BlueprintReadWrite, Category = "Simple Counter")
		FColor SimpleCounterColor;
	UPROPERTY(BlueprintReadWrite, Category = "Simple Counter")
		bool bUseSafeVariable = true;
	//Simple Counter Setting

	//Simple Counter Controll
	UFUNCTION(BlueprintCallable)
		void StopSimpleCounter();
	UFUNCTION(BlueprintCallable)
		void KillSimpleCounter(bool bIsShouldToWait);
	UFUNCTION(BlueprintCallable)
		void CreateSimpleCounterThread();
	UFUNCTION(BlueprintCallable)
		bool SwitchRunStateSimpleCounterThread(bool bIsPause);
	UFUNCTION(BlueprintPure)
		int64 GetCounters_SimpleCounter();

	//Events
	UPROPERTY(BlueprintReadWrite, Category = "Simple Counter")
		bool bIsUseEvent = true;
	
public:
	FEvent* SimpleCounterEvent = nullptr;

	UFUNCTION(BlueprintCallable)
		void TriggerSimpleCounterThreadWithEvent();
	UFUNCTION(BlueprintCallable)
		void TriggerSimpleCounterThreadWithScopedEvent();
	//Events


	//Scoped Events
public:
	UPROPERTY(BlueprintReadWrite, Category = "Simple Counter")
		bool bIsUseScopedEvent = true;
	FScopedEvent* SimpleCounterScopedEvent_Ref = nullptr;

	void SendRef_ScopedEvent(FScopedEvent& EventRef)
	{
		SimpleCounterScopedEvent_Ref = &EventRef;
	}
	//Scoped Events
	
	//Simple Counter Controll



	//Simple Mutex Settings

	TArray<FRunnableThread*> CurrentArrayRunningGameModeThread_SimpleMutex;
	FRunnableThread* CurrentRunningGameModeThread_SimpleMutex;

	UPROPERTY(BlueprintReadWrite, Category = "Simple Mutex")
		int32 NumberThread_SimpleMutex = 10;

	UPROPERTY(BlueprintReadWrite, Category = "Simple Counter")
		FColor SimpleMutexColor;
	//Simple Mutex Settings
	
	//Simple Mutex Controll
	UFUNCTION(BlueprintCallable)
		void CreateSimpleMutexThread();
	UFUNCTION(BlueprintCallable)
		void CreateSimpleCollectorThread();
	UFUNCTION(BlueprintCallable)
		void StopSimpleMutexThread();

	UFUNCTION(BlueprintCallable)
		TArray<FString> GetSecondsName();
	UFUNCTION(BlueprintCallable)
		TArray<FString> GetFirstNames();
	UFUNCTION(BlueprintCallable)
		TArray<FNPCInfoStruct> GetNPCInfo();

	float GetCurrentTime();
	//Simple Mutex Controll

	//Simple Mutex Storage

	TArray<FString> FirstName;
	FCriticalSection FirstName_Mutex;
	//��������� ������
	TQueue<FString, EQueueMode::Mpsc> SecondName;
	TArray<FString> CurrentSecondName;

	FCriticalSection NPCName_Mutex;
	TArray<FNPCInfoStruct> NPCInfo;

	void EventMessage_NameGenerator(bool bIsSecond, FString StringData);
	void EventMessage_NPCInfo(FNPCInfoStruct NPCInfoData);
	int32 CountCube = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Simple Mutex")
		TSubclassOf<class ASimpleCubeActor> CubeClass;



	// Paralell For

	UFUNCTION(BlueprintCallable)
	void StartParallel_1();
	UPROPERTY(BlueprintReadOnly)
	int32 CountParallel_1 = 0;
	
	UFUNCTION(BlueprintCallable)
		void StartParallel_2();
	UPROPERTY(BlueprintReadOnly)
		int32 CountParallel_2 = 0;
	UFUNCTION(BlueprintCallable)
		void StartParallel_3();
	UPROPERTY(BlueprintReadOnly)
		int32 CountParallel_3 = 0;

};