// Copyright Epic Games, Inc. All Rights Reserved.

#include "ThreadMiddle.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ThreadMiddle, "ThreadMiddle" );
