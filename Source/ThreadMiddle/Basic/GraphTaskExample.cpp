// Fill out your copyright notice in the Description page of Project Settings.


#include "../Basic/GraphTaskExample.h"
#include"Kismet/GameplayStatics.h"
// Sets default values
AGraphTaskExample::AGraphTaskExample()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGraphTaskExample::BeginPlay()
{
	Super::BeginPlay();
	TaskDelegat_OnWorkDone.BindUFunction(this, FName("OnWork_Done"));
	AThreadMiddleGameModeBase* CurrentGameMode = Cast<AThreadMiddleGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	if(CurrentGameMode)
	HoldTask = TGraphTask<FTask_CounterWork>::CreateTask(nullptr, ENamedThreads::AnyThread).ConstructAndHold(TaskDelegat_OnWorkDone, CurrentGameMode, &CounterTask, FPlatformTLS::GetCurrentThreadId());

}

// Called every frame
void AGraphTaskExample::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGraphTaskExample::OnWork_DoneBP_Implementation(int32 Result)
{
	//BP
}

void AGraphTaskExample::OnWork_Done(int32 Result)
{
	HoldTask = nullptr;
	OnWork_DoneBP(Result);
}

void AGraphTaskExample::StartAsyncWork()
{
	if (HoldTask)
	{
		if (!HoldTask->GetCompletionEvent().IsValid())
		{
			HoldTask->Unlock();
		}
	}
	else
	{
		AThreadMiddleGameModeBase* CurrentGameMode = Cast<AThreadMiddleGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
		if (CurrentGameMode)
		TGraphTask<FTask_CounterWork>::CreateTask(nullptr, ENamedThreads::AnyThread).ConstructAndDispatchWhenReady(TaskDelegat_OnWorkDone, CurrentGameMode, &CounterTask, FPlatformTLS::GetCurrentThreadId());
	}
}

