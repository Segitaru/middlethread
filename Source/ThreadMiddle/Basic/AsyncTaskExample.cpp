// Fill out your copyright notice in the Description page of Project Settings.


#include "../Basic/AsyncTaskExample.h"
#include "HAL/ThreadManager.h"


class FAsyncTask_Counter : public FNonAbandonableTask
{
	friend class FAutoDeleteAsyncTask<FAsyncTask_Counter>;

	int32 ExampleData;
	FAsyncTask_OnWorkDone AsyncTask_OnWorkDone;
	int32 *SimpleOutput;

	FAsyncTask_Counter(int32 InExampleData, FAsyncTask_OnWorkDone InAsyncTask_OnWorkDone, int32 *InOutSimpleData)
	: ExampleData(InExampleData), AsyncTask_OnWorkDone(InAsyncTask_OnWorkDone), SimpleOutput(InOutSimpleData) // CAUTION!: Must not use references in the constructor args; use pointers instead if you need by reference
	{
		UE_LOG(LogTemp, Error, TEXT("FAsyncTask_Counter::FTask_FinishWork()"))// Usually the constructor doesn't do anything except save the arguments for use in DoWork or GetDesiredThread.
	}
	~FAsyncTask_Counter()
	{
		UE_LOG(LogTemp, Error, TEXT("FAsyncTask_Counter::~FTask_FinishWork()"))// you will be destroyed immediately after you execute. Might as well do cleanup in DoWork, but you could also use a destructor.
	}
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FAsyncTask_Counter, STATGROUP_ThreadPoolAsyncTasks);
	}

	//static ENamedThreads::Type GetDesiredThread() { return ENamedThreads::GameThread; }

	//static ESubsequentsMode::Type GetSubsequentsMode() { return ESubsequentsMode::FireAndForget; }

	void DoWork()
	{
		uint64 i = 0;
		while (i < 200)
		{
			FPlatformProcess::Sleep(0.02f);
			*SimpleOutput = (*SimpleOutput + 1);
			i++;
		}
	}


};
// Sets default values
AAsyncTaskExample::AAsyncTaskExample()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAsyncTaskExample::BeginPlay()
{
	Super::BeginPlay();
	AsyncTask(ENamedThreads::AnyBackgroundThreadNormalTask, [&]()
		{
			int32 i = 0;
			while (i < 50)
			{
				FPlatformProcess::Sleep(0.02f);
				i++;
				Counter_2 = i;
			}

		});
}

// Called every frame
void AAsyncTaskExample::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAsyncTaskExample::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void AAsyncTaskExample::StartExample(bool InBackGroundTask /*= true*/)
{
	if (InBackGroundTask)
	{
		(new FAutoDeleteAsyncTask<FAsyncTask_Counter>(5, AsyncTask_OnWorkDone, &Counter_1))->StartBackgroundTask();
	}
	else
	{
		(new FAutoDeleteAsyncTask<FAsyncTask_Counter>(5, AsyncTask_OnWorkDone, &Counter_1))->StartSynchronousTask();
	}
}

