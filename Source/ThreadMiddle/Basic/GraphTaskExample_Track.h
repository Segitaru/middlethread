// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GraphTaskExample_Track.generated.h"

DECLARE_DELEGATE_OneParam(FTaskDelegatTrack_OnWorkDone, int32 OutResult);


UCLASS()
class THREADMIDDLE_API AGraphTaskExample_Track : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGraphTaskExample_Track();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void StartAsyncTask();
	UPROPERTY(BlueprintReadOnly)
	int32 Counter =0;
	FCriticalSection CounterMutex;

	FTaskDelegatTrack_OnWorkDone TaskDelegatTrack_OnWorkDone;
	UFUNCTION()
	void OnWorkDone(int32 Result);
	UFUNCTION(BlueprintNativeEvent)
	void OnWorkDoneBP(int32 Result);
};
