// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../ThreadMiddleGameModeBase.h"
#include "GraphTaskExample.generated.h"

DECLARE_DELEGATE_OneParam(FTaskDelegat_OnWorkDone, int32 OutResult);

class FTask_FinishWork
{
	FTaskDelegat_OnWorkDone TaskDelegat_OnWorkDone;
	AThreadMiddleGameModeBase* GameModeRef = nullptr;
	int32 Result = 0;
	
public:
	FTask_FinishWork(FTaskDelegat_OnWorkDone InTaskDelegat_OnWorkDone,
		int32 InSimpleOut) // CAUTION!: Must not use references in the constructor args; use pointers instead if you need by reference
		: TaskDelegat_OnWorkDone(InTaskDelegat_OnWorkDone), Result(InSimpleOut)
	{
		UE_LOG(LogTemp, Error, TEXT("FTask_Counter::FTask_FinishWork()"))// Usually the constructor doesn't do anything except save the arguments for use in DoWork or GetDesiredThread.
	}
	~FTask_FinishWork()
	{
		UE_LOG(LogTemp, Error, TEXT("FTask_Counter::~FTask_FinishWork()"))// you will be destroyed immediately after you execute. Might as well do cleanup in DoWork, but you could also use a destructor.
	}
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FTask_FinishWork, STATGROUP_TaskGraphTasks);
	}

	static ENamedThreads::Type GetDesiredThread() { return ENamedThreads::GameThread; }

	static ESubsequentsMode::Type GetSubsequentsMode() { return ESubsequentsMode::FireAndForget; }
	
	void DoTask(ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent)
	{
		check(IsInGameThread())
		{
			if (TaskDelegat_OnWorkDone.IsBound())
			{
				TaskDelegat_OnWorkDone.Execute(Result);
			}
		}
	}
};
class FTask_CounterWork
{
	FTaskDelegat_OnWorkDone TaskDelegat_OnWorkDone;
	AThreadMiddleGameModeBase* GameModeRef = nullptr;
	int32	*SimpleOut = nullptr;
	int32 Number;
public:
	FTask_CounterWork(FTaskDelegat_OnWorkDone InTaskDelegat_OnWorkDone, 
		AThreadMiddleGameModeBase* InThreadMiddleGameModeBase,
		int32* InSimpleOut, int32 InNumber) // CAUTION!: Must not use references in the constructor args; use pointers instead if you need by reference
		: TaskDelegat_OnWorkDone(InTaskDelegat_OnWorkDone),
		GameModeRef(InThreadMiddleGameModeBase),
		SimpleOut(InSimpleOut),	Number(InNumber)
	{
		UE_LOG(LogTemp, Error, TEXT("FTask_Counter::FTaskCounter"))// Usually the constructor doesn't do anything except save the arguments for use in DoWork or GetDesiredThread.
	}
	~FTask_CounterWork()
	{
		UE_LOG(LogTemp,Error, TEXT("FTask_Counter::~FTaskCounter"))// you will be destroyed immediately after you execute. Might as well do cleanup in DoWork, but you could also use a destructor.
	}
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FTask_CounterWork, STATGROUP_TaskGraphTasks);
	}

	static ENamedThreads::Type GetDesiredThread()
	{
		FAutoConsoleTaskPriority PriorityCustom(
			TEXT("FTask_Counter"),
			TEXT("FTask_Counter - In Working"),
			ENamedThreads::BackgroundThreadPriority, // if we have background priority task threads, then use them...
			ENamedThreads::NormalTaskPriority, // .. at normal task priority
			ENamedThreads::NormalTaskPriority // if we don't have background threads, then use normal priority threads at normal task priority instead
		);
		return PriorityCustom.Get();
	}

	static ESubsequentsMode::Type GetSubsequentsMode() { return ESubsequentsMode::FireAndForget; }
	void AnyThreadWork()
	{
		int32 i = 0;
		while (i<50)
		{
			FPlatformProcess::Sleep(0.1f);
			*SimpleOut = (*SimpleOut + 1);
			i++;
		}
		
	}
	void DoTask(ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent)
	{
		AnyThreadWork();

		TGraphTask<FTask_FinishWork>::CreateTask(nullptr, CurrentThread).ConstructAndDispatchWhenReady(TaskDelegat_OnWorkDone, *SimpleOut);
		// The arguments are useful for setting up other tasks. 
		// Do work here, probably using SomeArgument.
		//MyCompletionGraphEvent->DontCompleteUntil(TGraphTask<FSomeChildTask>::CreateTask(NULL, CurrentThread).ConstructAndDispatchWhenReady());
		
	
	}
};


UCLASS(BlueprintType)
class THREADMIDDLE_API AGraphTaskExample : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGraphTaskExample();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	FTaskDelegat_OnWorkDone TaskDelegat_OnWorkDone;
	UPROPERTY(BlueprintReadOnly)
	int32 CounterTask = 0;

	UFUNCTION(BlueprintNativeEvent)
	void OnWork_DoneBP(int32 Result);
	UFUNCTION()
	void OnWork_Done(int32 Result);

	TGraphTask<FTask_CounterWork>* HoldTask = nullptr;

	UFUNCTION(BlueprintCallable)
	void StartAsyncWork();
};

