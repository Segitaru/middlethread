// Fill out your copyright notice in the Description page of Project Settings.


#include "../Basic/GraphTaskExample_Track.h"



class FTask_FinishedWork_Track
{
	FTaskDelegatTrack_OnWorkDone TaskDelegatTrack_OnWorkDone;
	int32 *Result;

public:
	FTask_FinishedWork_Track(FTaskDelegatTrack_OnWorkDone InTaskDelegatTrack_OnWorkDone,
	int32 *InResult) // CAUTION!: Must not use references in the constructor args; use pointers instead if you need by reference
		: TaskDelegatTrack_OnWorkDone(InTaskDelegatTrack_OnWorkDone), Result(InResult)
	{
		UE_LOG(LogTemp, Error, TEXT("FTask_FinishedWork_Track::FTask_FinishWork()"))// Usually the constructor doesn't do anything except save the arguments for use in DoWork or GetDesiredThread.
	}
	~FTask_FinishedWork_Track()
	{
		UE_LOG(LogTemp, Error, TEXT("FTask_FinishedWork_Track::~FTask_FinishWork()"))// you will be destroyed immediately after you execute. Might as well do cleanup in DoWork, but you could also use a destructor.
	}
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FTask_FinishedWork_Track, STATGROUP_TaskGraphTasks);
	}

	static ENamedThreads::Type GetDesiredThread() { return ENamedThreads::GameThread; }

	static ESubsequentsMode::Type GetSubsequentsMode() { return ESubsequentsMode::TrackSubsequents; }

	void DoTask(ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent)
	{
		if (TaskDelegatTrack_OnWorkDone.IsBound())
			{
				TaskDelegatTrack_OnWorkDone.Execute(*Result);
			}
		
	}
};

class FTask_Counter_Track
{
	FCriticalSection* MutexCounter;
	int32* Result;

public:
	FTask_Counter_Track(FCriticalSection* InMutexCounter,
		int32* InResult) // CAUTION!: Must not use references in the constructor args; use pointers instead if you need by reference
		: MutexCounter(InMutexCounter), Result(InResult)
	{
		UE_LOG(LogTemp, Error, TEXT("FTask_Counter_Track::FTask_FinishWork()"))// Usually the constructor doesn't do anything except save the arguments for use in DoWork or GetDesiredThread.
	}
	~FTask_Counter_Track()
	{
		UE_LOG(LogTemp, Error, TEXT("FTask_Counter_Track::~FTask_FinishWork()"))// you will be destroyed immediately after you execute. Might as well do cleanup in DoWork, but you could also use a destructor.
	}
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FTask_Counter_Track, STATGROUP_TaskGraphTasks);
	}

	static ENamedThreads::Type GetDesiredThread()
	{
		FAutoConsoleTaskPriority TaslPriority(TEXT("TaskGraph.TaskPriority.LoadFileToString"), TEXT("Task and thread priority for file loading"), ENamedThreads::BackgroundThreadPriority, ENamedThreads::NormalTaskPriority, ENamedThreads::NormalTaskPriority);
		return TaslPriority.Get();
	}

	static ESubsequentsMode::Type GetSubsequentsMode() { return ESubsequentsMode::TrackSubsequents; }

	void DoTask(ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent)
	{
		for (int i = 0; i < 50; i++)
		{
			FPlatformProcess::Sleep(0.02f);
			FScopeLock ScopedLock(MutexCounter);
			*Result = (* Result + 1);
		}
	}
};
// Sets default values
AGraphTaskExample_Track::AGraphTaskExample_Track()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGraphTaskExample_Track::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGraphTaskExample_Track::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGraphTaskExample_Track::StartAsyncTask()
{
	FGraphEventArray prerequilsites;
	for (int i = 0; i < 4; i++)
	{
		FGraphEventRef CounterTask = TGraphTask<FTask_Counter_Track>::CreateTask(nullptr, ENamedThreads::AnyThread).ConstructAndDispatchWhenReady(&CounterMutex ,&Counter);
		prerequilsites.Add(CounterTask);
	}
	TaskDelegatTrack_OnWorkDone.BindUFunction(this, "OnWorkDone");
	TGraphTask<FTask_FinishedWork_Track>::CreateTask(&prerequilsites, ENamedThreads::AnyThread).ConstructAndDispatchWhenReady(TaskDelegatTrack_OnWorkDone,&Counter);
}

void AGraphTaskExample_Track::OnWorkDone(int32 Result)
{
	OnWorkDoneBP(Result);
}

void AGraphTaskExample_Track::OnWorkDoneBP_Implementation(int32 Result)
{

}

